Parallel-in-time repository
---------------------------

Contains source code related to time-parallel algorithms

* simTimeComm: contains C++ programm to measure time-communication cost when space-time domain 
decomposition is used on structured 3D meshes.
 
