simTimeComm directory
=====================

Contains source code, compilation and run script for a C++ programm aiming at measuring
time-communication cost when space-time domain decomposition is used on 3D structured meshes.
If `compile_mpic++` or `compile_mpiicpc` command are

* main.cpp: C++ source code
* compile_mpic++: bash command for compilation with GNU compiler
* compile_mpiicpc: bash command for compilation with Intel compiler
* run_slurm.sh: example of [SLURM](https://slurm.schedmd.com/sbatch.html) bash script

Example of use
--------------

```shell
./compile_mpic++
mpirun -n 10 simTimeComm 80 80 80 5
```

