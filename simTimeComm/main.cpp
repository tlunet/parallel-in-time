/**
    SimTimeComm, main.cpp
    Purpose: Simulate space-time domain decompositon and measure communication time between time processes.
    Arguments:
      nXGlob   --> number of points in X direction, 
      nYGlob   --> number of points in Y direction,
      nZGlob   --> number of points in Z direction,
      nFields  --> number of field variables.

    The number of process (should be even) is set through the mpirun (or mpiexec) command.

    @author Thibaut Lunet
    @version 1.2 06/05/2017
*/
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h>
#include <new>
#include <stdarg.h>

//!  Space-Time communication class.
/*!
  This class creates a space time-domain decomposition between 2N processes,

  |P[0]|  |P[1]|  ...  |P[N-1]| => time domain 0
  |P[N]| |P[N+1]| ... |P[2N-1]| => time domain 1
  
  with P[0], P[N] one space domain, P[1], P[N+1] an other one, etc ...
  Space domain decomposition is done on a 3D structured grid.

  This class implements methods to perform one MPI communication between time-domain
  (between P[0] and P[N], P[1] and P[N+1], etc ...).
*/
class SpaceTimeComm {
  public :
  
  //! Process rank in global, space and time MPI COMM_WORLD.
  int globRank, spaceRank, timeRank;
  //! Size of global, space and time MPI COMM_WORLD.
  int globSize, spaceSize, timeSize;
  //! Number of processes in space, time MPI COMM_WORLD.
  int nProcSpace, nProcTime;
  //! Process coloration in space, time MPI COMM_WORLD.
  int spaceColor, timeColor;
  
  //! MPI stuff.
  MPI_Comm spaceComm, timeComm;
  MPI_Request sendReq, recvReq;
  MPI_Status status;

  //! MPI communication time.
  double tRecv, tIrecv, tSend, tIsend;
  //! Statistics (see documentation of reduceStats() method).
  double stats[15];

  //! Number of points in x, y, z direction.
  int nXLoc, nYLoc, nZLoc;
  //! Number of space processes in x, y, z direction.
  int nXProc, nYProc, nZProc;
  //! Number of point of the local space domain.
  int nPoints;

  //! Constructor.
  /*!
    Generate the space-time processes decomposition.
  */
  SpaceTimeComm() {

    // Global communicator.
    MPI_Comm_size(MPI_COMM_WORLD, &globSize);
    MPI_Comm_rank(MPI_COMM_WORLD, &globRank);
    if (globSize % 2 != 0) {
      exitError("Global communicator size is not an even number");
    }

    // Build time and space communicator.
    nProcTime = 2;
    nProcSpace = globSize / nProcTime;
    spaceColor = (globRank-globRank % nProcSpace)/nProcSpace;
    timeColor = globRank % nProcSpace;
    MPI_Comm_split(MPI_COMM_WORLD, spaceColor, globRank, &spaceComm);
    MPI_Comm_split(MPI_COMM_WORLD, timeColor, globRank, &timeComm);
    MPI_Comm_rank(spaceComm, &spaceRank);
    MPI_Comm_rank(timeComm, &timeRank);
    MPI_Comm_size(spaceComm, &spaceSize);
    MPI_Comm_size(timeComm, &timeSize);

    // Print communicators repartition.
    rootMsg("Communicators settings : gSize=%d, tSize=%d, sSize=%d", globSize, timeSize, spaceSize);

    // Initialize stats.
    tRecv = 0.0; 
    tIrecv = 0.0;
    tSend = 0.0;
    tIsend = 0.0;
    for (int i=0; i<15; i++){
      stats[i] = -1.0;
    }

  }

  ~SpaceTimeComm() {}
  void Delete() {delete this;}

  //! Print an error message (ony root process P0) and terminate MPI run.
  void exitError(const char *fmt, ...) {
    MPI_Barrier(MPI_COMM_WORLD);
    if ( globRank == 0 ) {
      char buffer [256];
      va_list args;
      va_start(args, fmt);
      vsprintf(buffer, fmt, args);
      printf(" -- Error : %s\n", buffer);
    }
    MPI_Finalize();
    exit(-1);
  }

  //! Print a message (only root process).
  void rootMsg(const char *fmt, ...) {
    MPI_Barrier(MPI_COMM_WORLD);
    if ( globRank == 0 ) {
      char buffer [256];
      va_list args;
      va_start(args, fmt);
      vsprintf(buffer, fmt, args);
      printf("%s\n", buffer);
    }
    MPI_Barrier(MPI_COMM_WORLD); 
  }

  //! Print a message (each rank).
  void rankMsg(const char *fmt, ...) {
    MPI_Barrier(MPI_COMM_WORLD);
    usleep(2000*globRank);
    char buffer [256];
    va_list args;
    va_start(args, fmt);
    vsprintf(buffer, fmt, args);
    printf(" -- (G%dT%dS%d) : %s\n", globRank, timeRank, spaceRank, buffer);
    MPI_Barrier(MPI_COMM_WORLD);
  }

  //! Send the local space field values to the other time-process, non blocking communications.
  void iSend(double* values, int nPoints) {
    double tStart, tEnd;
    MPI_Isend(values, nPoints, MPI_DOUBLE, 1, spaceRank, timeComm, &sendReq);
    tStart = MPI_Wtime();
    MPI_Wait(&sendReq, &status);
    tEnd = MPI_Wtime();
    tIsend = tEnd-tStart;
  }

  //! Receive the local space field values from the other time-process, non blocking communications.
  void iRecv(double* values, int nPoints) {
    double tStart, tEnd;
    MPI_Irecv(values, nPoints, MPI_DOUBLE, 0, spaceRank, timeComm, &recvReq);
    tStart = MPI_Wtime();
    MPI_Wait(&recvReq, &status);
    tEnd = MPI_Wtime();
    tIrecv = tEnd-tStart;
  }

  //! Send the local space field values to the other time-process, blocking communications.
  void send(double* values, int nPoints) {
    double tStart, tEnd;
    tStart = MPI_Wtime();
    MPI_Send(values, nPoints, MPI_DOUBLE, 1, spaceRank, timeComm);
    tEnd = MPI_Wtime();
    tSend = tEnd-tStart;
  }

  //! Receive the local space field values from the other time-process, non blocking communications.
  void recv(double* values, int nPoints) {
    double tStart, tEnd;
    tStart = MPI_Wtime();
    MPI_Recv(values, nPoints, MPI_DOUBLE, 0, spaceRank, timeComm, &status);
    tEnd = MPI_Wtime();
    tRecv = tEnd-tStart;
  }

  //! Reduce statistics.
  /*!
    Reduce all statistics for MPI communication time into the stats attribute of
    root (P[0]) process.

    stats[0] : min time for non-blocking send,
    stats[1] : max time for non-blocking send,
    stats[2] : mean time for non-blocking send,
    stats[3] : min time for non-blocking receive,
    stats[4] : max time for non-blocking receive,
    stats[5] : mean time for non-blocking receive,
    stats[6] : min time for blocking send,
    stats[7] : max time for blocking send,
    stats[8] : mean time for blocking send,
    stats[9] : min time for blocking receive,
    stats[10] : max time for blocking receive,
    stats[11] : mean time for blocking receive,
    stats[12] : min number of point in local space domains,
    stats[13] : max number of point in local space domains,
    stats[14] : mean number of point in local space domains.

    @param nFields the number of field variable
  */
  void reduceStats(){

    // Non-blocking communication statistics.
    if (timeRank == 0){
      MPI_Reduce(&tIsend, &stats[0], 1, MPI_DOUBLE, MPI_MIN, 0, spaceComm);
      MPI_Reduce(&tIsend, &stats[1], 1, MPI_DOUBLE, MPI_SUM, 0, spaceComm);
      stats[1] /= spaceSize;
      MPI_Reduce(&tIsend, &stats[2], 1, MPI_DOUBLE, MPI_MAX, 0, spaceComm);
      if (spaceRank == 0){
        // Receive tIrecv stats
        MPI_Recv(&stats[3], 3, MPI_DOUBLE, 1, 345, timeComm, &status) ;
      }
    } else {
      MPI_Reduce(&tIrecv, &stats[3], 1, MPI_DOUBLE, MPI_MIN, 0, spaceComm);
      MPI_Reduce(&tIrecv, &stats[4], 1, MPI_DOUBLE, MPI_SUM, 0, spaceComm);
      stats[4] /= spaceSize;
      MPI_Reduce(&tIrecv, &stats[5], 1, MPI_DOUBLE, MPI_MAX, 0, spaceComm);
      if (spaceRank == 0){
        // Send tIrecv stats
        MPI_Send(&stats[3], 3, MPI_DOUBLE, 0, 345, timeComm) ;
      }
    }

    // Blocking communication statistics.
    if (timeRank == 0){
      MPI_Reduce(&tSend, &stats[6], 1, MPI_DOUBLE, MPI_MIN, 0, spaceComm);
      MPI_Reduce(&tSend, &stats[7], 1, MPI_DOUBLE, MPI_SUM, 0, spaceComm);
      stats[7] /= spaceSize;
      MPI_Reduce(&tSend, &stats[8], 1, MPI_DOUBLE, MPI_MAX, 0, spaceComm);
      if (spaceRank == 0){
        // Receive tRecv stats
        MPI_Recv(&stats[9], 3, MPI_DOUBLE, 1, 91011 , timeComm, &status) ;
      }
    } else {
      MPI_Reduce(&tRecv, &stats[9], 1, MPI_DOUBLE, MPI_MIN, 0, spaceComm);
      MPI_Reduce(&tRecv, &stats[10], 1, MPI_DOUBLE, MPI_SUM, 0, spaceComm);
      stats[10] /= spaceSize;
      MPI_Reduce(&tRecv, &stats[11], 1, MPI_DOUBLE, MPI_MAX, 0, spaceComm);
      if (spaceRank == 0){
        // Send tRecv stats
        MPI_Send(&stats[9], 3, MPI_DOUBLE, 0, 91011, timeComm);
      }
    }

    // Local number of points statistics.
    if (timeRank == 0){
      double _nPoints = (double) nPoints;
      MPI_Reduce(&_nPoints, &stats[12], 1, MPI_DOUBLE, MPI_MIN, 0, spaceComm);
      MPI_Reduce(&_nPoints, &stats[13], 1, MPI_DOUBLE, MPI_SUM, 0, spaceComm);
      stats[13] /= spaceSize;
      MPI_Reduce(&_nPoints, &stats[14], 1, MPI_DOUBLE, MPI_MAX, 0, spaceComm);
    }
        
  }

  //! Print statistics.
  void printStats(){
    rootMsg(
      "Message size (Min Mean Max) :\n %e %e %e",
      stats[12], stats[13], stats[14]);
    rootMsg(
      "Communication time statistics (Min Mean Max) :\n tIsend %e %e %e\n iIrevc %e %e %e\n tSend  %e %e %e\n tRecv  %e %e %e",
      stats[0], stats[1], stats[2], stats[3], stats[4], stats[5], stats[6], stats[7], stats[8], stats[9], stats[10], stats[11]);
  }

  //! Set the space domain block decomposition, matching Hybrid CFD solver space decomposition.
  /*!
      @param nXGlob the global number of points in X direction.
      @param nYGlob the global number of points in Y direction.
      @param nZGlob the global number of points in Z direction.
      @param nFields the number of field variables. 
    */
  void setBlockDecomposition(const int nXGlob, const int nYGlob, const int nZGlob, const int nFields) {

    // Factor spaceSize into facs[0]^exps[0] * facs[1]^exps[1] * facs[2]^exps[2]
    int rest = spaceSize;
    int facs[3] = { 2 , 3 , 1 };
    int exps[3] = { 0 , 0 , 0 };
    for (int n=0 ; n < 2 ; n++) {
      while ( rest % facs[n] == 0 ) {
        exps[n] += 1;
        rest = rest / facs[n];
      }
    }      
    if ( rest > 1 ) {
      facs[2] = rest;
      exps[2] = 1;
    }

    //  Repeatedly slice domain, find Nblks[3]
    int Nblks[3] = { 1 , 1 , 1 } ;
    int Npts[3] = {nXGlob, nYGlob, nZGlob};
    if ( spaceSize > 1 ) {
      for (int n=2 ; n >= 0 ; n--) {
        while ( exps[n] > 0 ) {
          int dummy;
          int dummymax = -1;
          int dmax = 0;
          for (int d=0; d < 3; d++) {
            dummy = ( Npts[d] + Nblks[d] - 1 ) / Nblks[d] ;
            if ( dummy >= dummymax ) {
              dummymax = dummy;
              dmax = d;
            }
          }
          Nblks[dmax] *= facs[n];
          exps[n] -= 1;
        }
      }
    }

    // Set the local domains
    nXProc = Nblks[0];
    nYProc = Nblks[1];
    nZProc = Nblks[2];
    nXLoc = nXGlob/nXProc;
    nYLoc = nYGlob/nYProc;
    nZLoc = nZGlob/nZProc;
    nPoints = nXLoc*nYLoc*nZLoc*nFields;
  }

};

//! Main prograpp
int main(int argc, char** argv) {
  // Initialize the MPI environment.
  MPI_Init(&argc, &argv);

  // Initialize the MPI environment object.
  SpaceTimeComm comm = SpaceTimeComm();  

  // Get programme arguments. 
  if (argc < 5) {
    comm.rootMsg("Usage : %s nXGlob nYGlob nZGlob nFields\n", argv[0]);
    MPI_Finalize();
    exit(-1);
  }
  int nXGlob = atoi(argv[1]);
  int nYGlob = atoi(argv[2]);
  int nZGlob = atoi(argv[3]);
  int nFields = atoi(argv[4]);

  // Set and print space-time domain decomposition
  comm.setBlockDecomposition(nXGlob, nYGlob, nZGlob, nFields);
  comm.rootMsg("Block decomposition : nXProc=%d, nYProc=%d, nZProc=%d", 
               comm.nXProc, comm.nYProc, comm.nZProc);
  comm.rankMsg("nXLoc=%d, nYLoc=%d, nZLoc=%d, nFields=%d, nPoints=%d", 
               comm.nXLoc, comm.nYLoc, comm.nZLoc, nFields, comm.nPoints);

   // Allocate memory for transfered data, and initialize data
  const int nPoints = comm.nPoints;
  double * values = new (std::nothrow) double [nPoints];
  if ( values == NULL ) {
    comm.exitError("Allocating memory");
  }
  for (int i=0; i < nPoints; i++) {
    values[i] = 1.23;
  }

  // Timing non-blocking communications
  comm.rootMsg("Non-Blocking Communication test");
  if (comm.timeRank == 0) {
    comm.iSend(values, nPoints);
    comm.rankMsg("tIsend=%e", comm.tIsend);
  } else {
    comm.iRecv(values, nPoints);
    comm.rankMsg("tIrecv=%e", comm.tIrecv);
  }
  // Timing blocking communications
  comm.rootMsg("Blocking Communication test");
  if (comm.timeRank == 0) {
    comm.send(values, nPoints);
    comm.rankMsg("tSend=%e", comm.tSend);
  } else {
    comm.recv(values, nPoints);
    comm.rankMsg("tRecv=%e", comm.tRecv);
  }

  // Compute statistics
  comm.reduceStats();
  comm.printStats();

  // Cleaning up
  if ( values != NULL ) {
    delete [] values ;
    values = NULL ;
  }
   
  // Finalize
  MPI_Finalize();
}
