#!/bin/bash

#SBATCH -J simTimeCom
#SBATCH -N 8
#SBATCH -n 160
#SBATCH --ntasks-per-node=20
#SBATCH --ntasks-per-core=1
#SBATCH --mail-user=t.lunet@isae.fr
#SBATCH --time=00:01:00
#SBATCH --cpu_bind=mask_cpu:0xfffff

nXGlob=80
nYGlob=80
nZGlob=80
nFields=5

mpirun -n ${SLURM_NTASKS} ./simTimeComm ${nXGlob} ${nYGlob} ${nZGlob} ${nFields}
